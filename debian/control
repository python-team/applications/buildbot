Source: buildbot
Section: devel
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Robin Jarry <robin@jarry.cc>
Standards-Version: 4.7.0
Build-Depends:
 debhelper-compat (= 13),
 dh-python (>= 3.20180313~),
 bash-completion,
 po-debconf,
 git,
 python3 (>= 3.5),
 python3-setuptools (>= 8.0),
 python3-alembic (>= 1.6.0),
 python3-autobahn (>= 0.16.0),
 python3-croniter,
 python3-dateutil (>= 1.5),
 python3-evalidate,
 python3-idna,
 python3-jinja2 (>= 2.1),
 python3-jwt,
 python3-openssl,
 python3-parameterized,
 python3-psutil,
 python3-service-identity,
 python3-sqlalchemy (>= 1.4.0),
 python3-twisted (>= 22.1.0),
 python3-txaio (>= 2.2.2),
 python3-zope.interface (>= 4.1.1),
 python3-treq (>= 20.9),
 python3-txrequests,
 python3-unidiff (>= 0.6.0),
 python3-yaml,
 python3-sphinx,
 python3-sphinx-jinja,
 python3-sphinx-rtd-theme,
 python3-docutils,
 sphinx-doc,
 sphinx-common,
 scdoc,
Homepage: https://buildbot.net
Vcs-Git: https://salsa.debian.org/python-team/packages/buildbot.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/buildbot
Rules-Requires-Root: no

Package: buildbot
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
 python3-alembic (>= 1.6.0),
 python3-autobahn (>= 0.16.0),
 python3-dateutil (>= 1.5),
 python3-idna,
 python3-jinja2 (>= 2.1),
 python3-jwt,
 python3-openssl,
 python3-service-identity,
 python3-sqlalchemy (>= 1.4.0),
 python3-treq (>= 20.9),
 python3-twisted (>= 22.1.0),
 python3-txaio (>= 2.2.2),
 python3-txrequests,
 python3-unidiff (>= 0.6.0),
 python3-zope.interface (>= 4.1.1),
 passwd,
Recommends:
 buildbot-worker,
 bash-completion,
Suggests:
 buildbot-doc,
 python3-evalidate,
 python3-lz4,
 python3-psycopg2 | python3-mysqldb,
Description: System to automate the compile/test cycle (server)
 The BuildBot is a system to automate the compile/test cycle required
 by most software projects to validate code changes. By automatically
 rebuilding and testing the tree each time something has changed,
 build problems are pinpointed quickly, before other developers are
 inconvenienced by the failure. The guilty developer can be identified
 and harassed without human intervention.
 .
 By running the builds on a variety of platforms, developers who do
 not have the facilities to test their changes everywhere before
 checkin will at least know shortly afterwards whether they have
 broken the build or not. Warning counts, lint checks, image size,
 compile time, and other build parameters can be tracked over time,
 are more visible, and are therefore easier to improve.
 .
 This package contains the master server.

Package: buildbot-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Breaks:
 buildbot (<< 1.0),
Replaces:
 buildbot (<< 1.0),
Description: System to automate the compile/test cycle (documentation)
 The BuildBot is a system to automate the compile/test cycle required
 by most software projects to validate code changes. By automatically
 rebuilding and testing the tree each time something has changed,
 build problems are pinpointed quickly, before other developers are
 inconvenienced by the failure. The guilty developer can be identified
 and harassed without human intervention.
 .
 By running the builds on a variety of platforms, developers who do
 not have the facilities to test their changes everywhere before
 checkin will at least know shortly afterwards whether they have
 broken the build or not. Warning counts, lint checks, image size,
 compile time, and other build parameters can be tracked over time,
 are more visible, and are therefore easier to improve.
 .
 This package contains the HTML documentation for both the master
 and worker.

Package: buildbot-worker
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
 python3-twisted (>= 21.2.0),
 passwd,
Recommends:
 brz | subversion | cvs | mercurial | darcs | git,
Description: System to automate the compile/test cycle (worker agent)
 The BuildBot is a system to automate the compile/test cycle required
 by most software projects to validate code changes. By automatically
 rebuilding and testing the tree each time something has changed,
 build problems are pinpointed quickly, before other developers are
 inconvenienced by the failure. The guilty developer can be identified
 and harassed without human intervention.
 .
 By running the builds on a variety of platforms, developers who do
 not have the facilities to test their changes everywhere before
 checkin will at least know shortly afterwards whether they have
 broken the build or not. Warning counts, lint checks, image size,
 compile time, and other build parameters can be tracked over time,
 are more visible, and are therefore easier to improve.
 .
 This package contains the worker, which performs the actual builds.
 It is recommended to run the worker on behalf of a non-privileged user.
